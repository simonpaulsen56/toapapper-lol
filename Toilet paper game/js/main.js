const App = {
  data() {
    return {
      //resources
      log: 
      {
          price: 15,
          amount: 0,
          buyamount: 1,
      },
      glue:
      {
        price: 5,
        amount: 0,
        buyamount: 1,
      },
      toiletpaper: 
      {
          amount: 0
      },
      money:
      {
        amount: 1000,
        debt: 0,

        maxmoneyreached: 0,
      },


      researchers:
      {
        totalwork: 0,
        amount: 0,
        productivity: 1,
        wage: 1,
        buyamount: 1,
        price: 50,

        maxresearchreached: 0,
      },



      time:
      {
        timepassed: 0,
        shortinterval: 1000,
        longinterval: 10000,
      },

        //research bonus multipliers
      multipliers:
      {
        woodfactorymultiplier_price: 1,
        woodfactorymultiplier_production: 1,
        woodfactorymultiplier_wage: 1,

        gluefactorymultiplier_price: 1,
        gluefactorymultiplier_production: 1,
        gluefactorymultiplier_wage: 1,

        paperfactorymultiplier_price: 1,
        paperfactorymultiplier_production: 1,
        paperfactorymultiplier_wage: 1,

        storemultiplier_sellprice: 1,
        storemultiplier_consumption: 1,
      },



      stats: 
      {
        woodproduction: 0,
        glueproduction: 0,
        paperproduction: 0,

        woodconsumption: 0,
        glueconsumption: 0,

        woodcost: 0,
        gluecost: 0,
        papercost: 0,
        totalcost: 0,

        sales: 0,
        papersold: 0,
      },

      //factories----------------------------------------------------------------
        //logcutters
      logcutter:
      [
        {
          name: "Small Loggery",
          amount: 0,
          price: 3000,

          wage: 50,
          production: 5,

          id: 0,
        },
        {
          name: "Large Loggery",
          amount: 0,
          price: 10000,

          wage: 100,
          production: 15,

          id: 1,
        },
        {
          name: "Treefarm",
          amount: 0,
          price: 40000,

          wage: 200,
          production: 40,

          id: 2,
        },
        {
          name: "Rainforrest Clearance Program",
          amount: 0,
          price: 100000,

          wage: 600,
          production: 150,

          id: 3,
        },
      ],
        //glue factories
      gluefactory:
      [
        {
          name: "Small Glue Factory",
          amount: 0,
          price: 8000,

          wage: 10,
          production: 4,
          id : 0,
        },
        {
          name: "Large Glue Factory",
          amount: 0,
          price: 80000,

          wage: 100,
          production: 80,
          id: 1,
        },
      ],
        //paper factories
      factory:
      [
        {
          name: "Basement",
          price: 100,
          amount: 0,
          wage: 50,

          logcost: 4,
          gluecost: 2,
          production: 80,
          id: 0,
        },
        {
          name: "small factory",
          price: 5000,
          amount: 0,
          wage: 100,

          logcost: 12,
          gluecost: 6,
          production: 250,
          id: 1,
        },
        {
          name: "big factory",
          price: 5000,
          amount: 0,
          wage: 100,

          logcost: 50,
          gluecost: 25,
          production: 1250,
          id: 2,
        },
      ],
        //stores / contracts
      store:
      [
        {
          name: "smal store",
          price: 80,
          amount: 0,

          consumption: 10,
          payment: 10,
          id: 0,
        },   
        {
          name: "big store",
          price: 800,
          amount: 0,

          consumption: 50,
          payment: 60,
          id: 1,
        },  
        {
          name: "smal store chain",
          price: 8000,
          amount: 0,

          consumption: 500,
          payment: 700,
          id: 2,
        },  
      ],

      //research
      tech: 
      [
        {
          id: 0,
          name: "log efficiency",
          effectname: "makes log production faster",
          effectdesc: "log amount: + 10%",
          price: 1,

          varname: "woodfactorymultiplier_production",
          varupdate: 0.1,

          isbought: false,
        },
        {
          id: 1,
          name: "log wages",
          effectname: "makes log production cheaper",
          effectdesc: "log wage: - 20%",
          price: 3,

          varname: "woodfactorymultiplier_wage",
          varupdate: -0.2,

          isbought: false,
        },
        {
          id: 2,
          name: "glue prod",
          effectname: "makes glue production faster",
          effectdesc: "glue production: + 25%",
          price: 3,

          varname: "gluefactorymultiplier_production",
          varupdate: 0.25,
          
          isbought: false,
        },
        {
          id: 3,
          name: "glue buyprice",
          effectname: "makes glue factories cheaper to buy",
          effectdesc: "glue factory price: -15%",
          price: 5,

          varname: "gluefactorymultiplier_price",
          varupdate: -0.15,
          
          isbought: false,
        },
        {
          id: 4,
          name: "glue buyprice",
          effectname: "makes glue factories even cheaper to buy",
          effectdesc: "glue factory price: -10%",
          price: 10,

          varname: "gluefactorymultiplier_price",
          varupdate: -0.1,
          
          isbought: false,
        },        {
          id: 5,
          name: "store consumption",
          effectname: "makes stores pay the same for less rolls",
          effectdesc: "store consumption: -10%",
          price: 15,

          varname: "storemultiplier_consumption",
          varupdate: -0.1,
          
          isbought: false,
        },
        
      ],



    };
  }, 
  methods: {
    // buy resources

    buytech(a,b,id)
    {
      if (this.researchers.totalwork >= this.tech[id].price)
      {
        this.multipliers[a] += b;
        console.log(a);
        this.tech[id].isbought = true;
        this.researchers.totalwork -= this.tech[id].price;
      }
    },

    buylog()
    {

      this.log.amount += Number(this.log.buyamount);
      this.money.amount -= this.log.price * this.log.buyamount;
      this.purchasefunctions();

    },
    buyglue()
    {
      this.glue.amount += Number(this.glue.buyamount);
      this.money.amount -= this.glue.price * this.glue.buyamount;
      this.purchasefunctions();
    },


    // buy/sell factories
    buylogcutter(index, bs)
    {
      if (bs == true && this.money.amount >= this.logcutter[index].price * this.multipliers.woodfactorymultiplier_price)
      {   
        this.logcutter[index].amount++;
        this.money.amount -= this.logcutter[index].price * this.multipliers.woodfactorymultiplier_price;
      }
      else if (bs == true)
      {
      }
      else
      {
        if (this.logcutter[index].amount > 0)
        {
          this.logcutter[index].amount--;
        }
      }
      this.purchasefunctions();
    },
    buygluefactory(index, bs)
    {
      if (bs == true && this.money.amount >= this.gluefactory[index].price * this.multipliers.gluefactorymultiplier_price)
      {   
        this.gluefactory[index].amount++;
        this.money.amount -= this.gluefactory[index].price * this.multipliers.gluefactorymultiplier_price;
      }
      else if (bs == 1)
      {
      }
      else
      {
        if (this.gluefactory[index].amount > 0)
        {
          this.gluefactory[index].amount--;
        }
      }
      this.purchasefunctions();
    },
    buyfactory(index, bs)
    {
      if (bs == true && this.money.amount >= this.factory[index].price * this.multipliers.paperfactorymultiplier_price)
      { 
        this.factory[index].amount++;
        this.money.amount -= this.factory[index].price * this.multipliers.paperfactorymultiplier_price;
      }
      else if (bs == 1)
      {

      }
      else
      {
        if (this.factory[index].amount > 0)
        {
          this.factory[index].amount--;
        }
      }
      this.purchasefunctions();
    },
    buystore(index, bs)
    {
      if (bs == true && this.toiletpaper.amount >= this.store[index].price)
      {
        this.store[index].amount++;
        this.toiletpaper.amount -= this.store[index].price;
      }
      else if (bs == true)
      {

      }
      else
      {
        if (this.store[index].amount > 0)
        {
          this.store[index].amount--;
        }
      }
      this.purchasefunctions();
    },
    buyresearchers(bs)
    {
      if (bs == true && this.money.amount >= this.researchers.price * this.researchers.buyamount)
      {
        this.researchers.amount += Number(this.researchers.buyamount);
        this.money.amount -= this.researchers.price * this.researchers.buyamount;
      }
      else if (bs == true)
      {

      }
      else
      {
        if (this.researchers.amount >= this.researchers.buyamount)
        {
          this.researchers.amount -= this.researchers.buyamount
        }
        else
        {
          this.researchers.amount = 0;
        }
      }
      this.purchasefunctions();
    },

    paydebt()
    {
      if (this.money.debt == 0)
      {

      }
      else if (this.money.debt <= this.money.amount)
      {
        this.money.amount -= Number(this.money.debt);
        this.money.debt = 0;
      }
      else
      {
        this.money.debt -= Number(this.money.amount);
        this.money.amount = 0;
      }
    },


    //get mad stats--------------------------------------------------
    getstats()
    {
      this.resetstats();


      this.getwoodstats();
      this.getgluestats();
      this.getfactorystats();
      this.getsalesstats();
      this.getmiscstats();

      this.stats.totalcost += this.stats.woodcost + this.stats.gluecost + this.stats.papercost;
    },

    //get stats subfunctions
    getwoodstats()
    {
      for (let i=0; i < this.logcutter.length; i++)
      {
        this.stats.woodproduction += this.logcutter[i].production * this.logcutter[i].amount * this.multipliers.woodfactorymultiplier_production;
        this.stats.woodcost += this.logcutter[i].wage * this.logcutter[i].amount * this.multipliers.woodfactorymultiplier_wage / this.time.longinterval * 1000;
      }
    },
    getgluestats()
    {
      for (let i=0; i < this.gluefactory.length; i++)
      {
        this.stats.glueproduction += this.gluefactory[i].production * this.gluefactory[i].amount * this.multipliers.gluefactorymultiplier_production;
        this.stats.gluecost += this.gluefactory[i].wage * this.gluefactory[i].amount * this.multipliers.gluefactorymultiplier_wage / this.time.longinterval * 1000;
      }
    },
    getfactorystats()
    {
      for (let i=0; i < this.factory.length; i++)
      {
        this.stats.paperproduction += this.factory[i].production * this.factory[i].amount * this.multipliers.paperfactorymultiplier_production;
        this.stats.papercost += this.factory[i].wage * this.factory[i].amount * this.multipliers.paperfactorymultiplier_wage / this.time.longinterval * 1000;

        this.stats.woodconsumption += this.factory[i].logcost * this.factory[i].amount;
        this.stats.glueconsumption += this.factory[i].gluecost * this.factory[i].amount;
      }
    },
    getsalesstats()
    {
      for (let i=0; i < this.store.length; i++)
      {
        this.stats.sales += this.store[i].payment * this.store[i].amount;
        this.stats.papersold += this.store[i].consumption * this.store[i].amount;
      }
    },
    getmiscstats()
    {
      this.stats.totalcost += this.researchers.amount * this.researchers.wage / this.time.longinterval * 1000;
    },

    //reset mad stats-------------------------------------------------
    resetstats()
    {   //https://zellwk.com/blog/looping-through-js-objects/
        const keys = Object.keys(this.stats)
        for(const key of keys)
        {
          this.stats[key] = 0;
        }
    },

    purchasefunctions()
    {
      this.getstats();
    },




    //per second fuctions-------------------------------------------
    producewood()
    {
      for (let i = 0; i < this.logcutter.length; i++)
      {
        this.log.amount += this.logcutter[i].production * this.logcutter[i].amount * this.multipliers.woodfactorymultiplier_production;
      }
    },
    produceglue()
    {
      for (let i = 0; i <this.gluefactory.length; i++)
      {
        this.glue.amount += this.gluefactory[i].production * this.gluefactory[i].amount * this.multipliers.gluefactorymultiplier_production;
      }
    },

    producepaper()
    {
      for(let i = 0; i < this.factory.length; i++)
      {
        if (this.log.amount >= this.factory[i].logcost * this.factory[i].amount &&
          this.glue.amount >= this.factory[i].gluecost * this.factory[i].amount)
        {
          this.log.amount -= this.factory[i].logcost * this.factory[i].amount;
          this.glue.amount -= this.factory[i].gluecost * this.factory[i].amount;
          this.toiletpaper.amount += this.factory[i].production * this.factory[i].amount  * this.multipliers.paperfactorymultiplier_production;
        }
      }
    },


    sellpaper()
    {
      for (let i = 0; i < this.store.length; i++)
      {
        if (this.toiletpaper.amount >= this.store[i].consumption * this.store[i].amount)
        {
          this.toiletpaper.amount -= this.store[i].consumption * this.store[i].amount;
          this.money.amount += this.store[i].payment * this.store[i].amount;
        }
      }
    },


    doresearch()
    {
      this.researchers.totalwork += this.researchers.amount * this.researchers.productivity / 1000;
      if (this.researchers.totalwork > this.stats.maxresearchreached)
      {
        this.stats.maxresearchreached = this.researchers.totalwork;
      }
    },

    //per minute functions----------------------------------------------------------------

    wages()
    {
      for (let i = 0; i < this.factory.length; i++)
      {
        this.money.amount -= this.factory[i].wage * this.factory[i].amount * this.multipliers.paperfactorymultiplier_wage;
      }
      for (let i = 0; i < this.logcutter.length; i++)
      {
        this.money.amount -= this.logcutter[i].wage * this.logcutter[i].amount * this.multipliers.paperfactorymultiplier_wage;
      }
      for (let i = 0; i < this.gluefactory.length; i++)
      {
        this.money.amount -= this.gluefactory[i].wage * this.gluefactory[i].amount * this.multipliers.gluefactorymultiplier_wage;
      }
      this.money.amount -= this.researchers.amount * this.researchers.wage;
    },
    debtadder()
    {
      if (this.money.amount < 0)
      {
        let inverse = this.money.amount * -1;
        this.money.debt += inverse;

        this.money.amount = 0;
      }
    },

    debtaccumulate()
    {
      if (this.money.amount > 0)
      {
        this.money.amount -= this.money.debt * 0.03;
        this.money.debt -= this.money.debt * 0.03;
      }

      this.money.debt *= 1.05;
    },
    miscmoney()
    {
      if (this.money.amount > this.stats.maxmoneyreached)
      {
        this.stats.maxmoneyreached = this.money.amount;
      }
    },


  },
  mounted() {
    //run all per second functions
    setInterval(() => 
    {
      this.producepaper();
      this.producewood();
      this.produceglue();
      this.sellpaper();
      this.debtadder();
      this.doresearch();
      this.miscmoney();
      this.time.timepassed++;
    }, this.time.shortinterval);
    //run all per minute functions
    setInterval(() => 
    {
      this.wages();
      this.debtaccumulate();
      //console.log(this.$data)

    }, this.time.longinterval)

  },
};

Vue.createApp(App).mount("#appBox");
